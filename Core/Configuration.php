<?php


class Configuration {

    // array of parameters
    private static $parameters;

    /**
     * @param $name string
     * @param $defaultValue null|mixed
     * @return mixed|null
     * @throws Exception
     */
    public static function get($name, $defaultValue = null) {
        if (isset(self::getParameters()[$name])) {
            $value = self::getParameters()[$name];
        }
        else {
            $value = $defaultValue;
        }
        return $value;
    }

    /**
     * create array from ini config file (dev.ini OR prod.ini) in this order
     * @return array|false
     * @throws Exception
     */
    private static function getParameters() {
        if (self::$parameters == null) {
            $cheminFichier = "Config/dev.ini";
            
            if (!file_exists($cheminFichier)) {
                $cheminFichier = "Config/prod.ini";
            }
            
            if (!file_exists($cheminFichier)) {
                throw new Exception("LBL_NO_CONFIG_FILE");
            }
            else {
                self::$parameters = parse_ini_file($cheminFichier);
            }
        }
        
        return self::$parameters;
    }
}

