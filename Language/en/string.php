<?php

$string_values = [
    "LBL_NO_ACTION" => "Missed action.",
    "LBL_NO_CONTROLLER" => "Controller not exist.",
    "LBL_NO_MODULE" => "Missed module.",
    "LBL_ACTION_NOT_EXIST" => "Action not exist.",
    "LBL_NO_VIEW" => "No view available.",
    "LBL_NO_CONFIG_FILE" => "No configuration file found",
    "LBL_CONNECTION_FAILED" => "Connection failed : ",
    "LBL_GO_BACK" => "Click to go back.",
    "LBL_TITLE_ERROR" => "Error",
    "LBL_TITLE_HOME" => "Home",
    "LBL_SEARCH" => "Search",
    "LBL_LANGUAGE" => "Language"
];